import { describe, it, expect } from "vitest";
import { render, screen } from "@testing-library/react";
import Toast, { ToastType } from "../../stories/Toast";

describe("Toast", () => {
  it("renders a success message", () => {
    render(<Toast type={ToastType.SUCCESS} message="Success message" />);
    expect(screen.getByText("Success message")).toBeInTheDocument();
    expect(screen.queryByTestId("cta-type")).toBeNull();
  });

  it("renders a success message with a CTA and description", () => {
    const description = "This is an important success message";
    render(
      <Toast
        type={ToastType.SUCCESS}
        message="Success with CTA"
        cta={true}
        description={description}
      />,
    );
    expect(screen.getByText("Success with CTA")).toBeInTheDocument();
    expect(screen.getByText(description)).toBeInTheDocument();
    expect(screen.getByTestId("cta-type")).toBeInTheDocument();
  });

  it("renders a danger message", () => {
    render(<Toast type={ToastType.DANGER} message="Danger message" />);
    expect(screen.getByText("Danger message")).toBeInTheDocument();
    expect(screen.queryByTestId("cta-type")).toBeNull();
  });

  it("renders a danger message with a CTA and description", () => {
    const description = "This is an important danger message";
    render(
      <Toast
        type={ToastType.DANGER}
        message="Danger with CTA"
        description={description}
        cta={true}
      />,
    );
    expect(screen.getByText("Danger with CTA")).toBeInTheDocument();
    expect(screen.getByText(description)).toBeInTheDocument();
    expect(screen.getByTestId("cta-type")).toBeInTheDocument();
  });

  it("renders an avatar message", () => {
    render(<Toast type={ToastType.AVATAR} message="Avatar message" />);
    expect(screen.getByText("Avatar message")).toBeInTheDocument();
    expect(screen.queryByTestId("cta-type")).toBeNull();
  });

  it("renders a message for mobile view", () => {
    render(
      <Toast
        type={ToastType.SUCCESS}
        message="Mobile view message"
        mobile={true}
      />,
    );
    // For a mobile view test, you might check for specific styling changes or layout differences.
    expect(screen.getByText("Mobile view message")).toBeInTheDocument();
    // Add additional checks for mobile-specific assertions if needed
  });

  it("renders a mobile view success message", () => {
    render(
      <Toast
        type={ToastType.SUCCESS}
        message="Mobile success message"
        mobile={true}
      />,
    );
    const mobileToast = screen.getByText("Mobile success message").parentElement
      ?.parentElement;
    expect(mobileToast).toBeInTheDocument();
    expect(mobileToast).toHaveStyle({ width: "341px" });
  });

  it("renders a mobile view success message with a CTA", () => {
    const description = "Mobile success message with CTA";
    render(
      <Toast
        type={ToastType.SUCCESS}
        message="Mobile CTA"
        description={description}
        cta={true}
        mobile={true}
      />,
    );
    const mobileToast =
      screen.getByText(description).parentElement.parentElement;
    expect(mobileToast).toBeInTheDocument();
    expect(mobileToast).toHaveStyle({ width: "341px" });
    expect(screen.getByTestId("cta-type")).toBeInTheDocument();
  });

  it("renders a mobile view danger message", () => {
    render(
      <Toast
        type={ToastType.DANGER}
        message="Mobile danger message"
        mobile={true}
      />,
    );
    const mobileToast = screen.getByText("Mobile danger message").parentElement
      ?.parentElement;
    expect(mobileToast).toBeInTheDocument();
    expect(mobileToast).toHaveStyle({ width: "341px" });
  });

  it("renders a mobile view danger message with a CTA and description", () => {
    const description = "Mobile danger message with CTA";
    render(
      <Toast
        type={ToastType.DANGER}
        message="Mobile danger CTA"
        description={description}
        cta={true}
        mobile={true}
      />,
    );
    const mobileToast =
      screen.getByText(description).parentElement.parentElement;
    expect(mobileToast).toBeInTheDocument();
    expect(mobileToast).toHaveStyle({ width: "341px" });
    expect(screen.getByTestId("cta-type")).toBeInTheDocument();
  });

  it("renders a mobile view avatar message with a CTA", () => {
    const message = "Mobile avatar message";
    const description = "Hi Neil, mobile avatar description.";
    render(
      <Toast
        type={ToastType.AVATAR}
        message={message}
        description={description}
        cta={true}
        mobile={true}
      />,
    );
    const mobileToast = screen.getByText(message).parentElement.parentElement;
    expect(mobileToast).toBeInTheDocument();
    expect(mobileToast).toHaveStyle({ width: "341px" });
    expect(screen.getByTestId("cta-type")).toBeInTheDocument();
  });
});
