import React from "react";
import Toast, { ToastType } from "./Toast";

export default {
  title: "Components/Toast",
  component: Toast,
};

const Template = (args) => <Toast {...args} />;

export const SuccessNoCTA = Template.bind({});
SuccessNoCTA.args = {
  type: ToastType.SUCCESS,
  message: "The action that you have done was a success! Well done",
};

export const SuccessWithCTALong = Template.bind({});
SuccessWithCTALong.args = {
  type: ToastType.SUCCESS,
  message: "Success",
  description:
    "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
  cta: true,
};

export const DangerNoCTA = Template.bind({});
DangerNoCTA.args = {
  type: ToastType.DANGER,
  message:
    "The file <strong>flowbite-figma-pro</strong> was permanently deleted.",
};

export const DangerWithCTALong = Template.bind({});
DangerWithCTALong.args = {
  type: ToastType.DANGER,
  message: "Attention",
  description:
    "Oh snap, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
  cta: true,
};

export const AvatarWithNameAndMessage = Template.bind({});
AvatarWithNameAndMessage.args = {
  type: ToastType.AVATAR,
  message: "Bonnie Green",
  description: "Hi Neil, thanks for sharing your thoughts regarding Flowbite.",
  cta: true,
};

export const SuccessNoCTAMobile = Template.bind({});
SuccessNoCTAMobile.args = {
  type: ToastType.SUCCESS,
  message: "The action that you have done was a success! Well done",
  mobile: true,
};

export const SuccessWithCTALongMobile = Template.bind({});
SuccessWithCTALongMobile.args = {
  type: ToastType.SUCCESS,
  message: "Success",
  description:
    "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
  cta: true,
  mobile: true,
};

export const DangerNoCTAMobile = Template.bind({});
DangerNoCTAMobile.args = {
  type: ToastType.DANGER,
  message:
    "The file <strong>flowbite-figma-pro</strong> was permanently deleted.",
  mobile: true,
};

export const DangerWithCTALongMobile = Template.bind({});
DangerWithCTALongMobile.args = {
  type: ToastType.DANGER,
  message: "Attention",
  description:
    "Oh snap, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
  cta: true,
  mobile: true,
};

export const AvatarWithNameAndMessageMobile = Template.bind({});
AvatarWithNameAndMessageMobile.args = {
  type: ToastType.AVATAR,
  message: "Bonnie Green",
  description: "Hi Neil, thanks for sharing your thoughts regarding Flowbite.",
  cta: true,
  mobile: true,
};
