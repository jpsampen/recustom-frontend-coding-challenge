import React from "react";
import Icon from "./Icon";
import "./Toast.css";
import avatarImg from "./assets/avatar.png";
import dangerImg from "./assets/danger.svg";
import dangerCtaImg from "./assets/danger_cta.svg";
import successImg from "./assets/success.svg";
import successCtaImg from "./assets/success_cta.svg";

export enum ToastType {
  SUCCESS = "success",
  DANGER = "danger",
  AVATAR = "avatar",
}

const colors = {
  success: "#00AC80",
  danger: "#FF6464",
  avatar: "#9CA3AF",
};

const images: { [K in ToastType]: string } = {
  [ToastType.SUCCESS]: successImg.src,
  [ToastType.DANGER]: dangerImg.src,
  [ToastType.AVATAR]: avatarImg.src,
};

const ctaImages: { [K in ToastType]?: string } = {
  [ToastType.SUCCESS]: successCtaImg.src,
  [ToastType.DANGER]: dangerCtaImg.src,
};

export interface ToastProps {
  type: ToastType;
  message: string;
  cta?: boolean;
  mobile?: boolean;
  description?: string;
}

const getWidth = (mobile: boolean) => (mobile ? "341px" : "606px");

const getImage = (type: ToastType, description?: string): string => {
  if (description && type in ctaImages) {
    return ctaImages[type]!;
  }
  return images[type];
};

const Toast = ({
  type,
  message,
  description,
  cta = false,
  mobile = false,
}: ToastProps) => {
  const startImage = getImage(type, description);
  const baseClass = "toast";

  return (
    <div
      className={`${baseClass} ${baseClass}--${type}`}
      style={{ width: getWidth(mobile) }}
    >
      <div
        className={`${baseClass}__message-wrapper`}
        style={{ alignItems: mobile ? "flex-start" : "center" }}
      >
        <img
          src={startImage}
          alt={type}
          className={`${baseClass}__start-image`}
          style={{ width: type === ToastType.AVATAR ? "32px" : "auto" }}
        />
        <div
          className={`${baseClass}__message ${baseClass}__message--${type}`}
          style={{
            width: mobile ? "271px" : "auto",
            fontWeight: cta ? 600 : "normal",
          }}
          dangerouslySetInnerHTML={{ __html: message }}
        />
        <Icon color={colors[type]} />
      </div>
      {description && (
        <div
          className={`${baseClass}__wrapper-desc ${baseClass}__wrapper-desc--${type}`}
        >
          <div className={`${baseClass}__description`}>{description}</div>
          {cta && (
            <button
              className={`${baseClass}__cta ${baseClass}__cta--${type}`}
              data-testid="cta-type"
            >
              {type === ToastType.AVATAR ? "Button Text" : "Take action"}
            </button>
          )}
        </div>
      )}
    </div>
  );
};

export default Toast;
